import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  indeks: number | undefined = undefined;
  // brojIndeksa : any = "1234/123456";
  student : any = {
    "brojIndeksa": undefined,
    ime: undefined,
    prezime: undefined,
    godinaUpisa: undefined,
    smer: undefined,
    "prosecnaOcena": undefined
  }
  title = 'Tabela studenata';

  smerovi : any[] = [
    {"sifraSmera": "SII", "naziv": "Softversko i informaciono inzenjerstvo"},
    {"sifraSmera": "IT", "naziv": "Informacione tehnologije"},

  ]
  studenti: any[] = [
    {
      "brojIndeksa": "1234/1234567",
      "ime": "Petar",
      "prezime": "Petrović",
      "smer": "SII",
    },
    {
      "brojIndeksa": "1235/1234567",
      "ime": "Marko",
      "prezime": "Petrović",
      "smer": "SII",
      "godinaUpisa": "test",
      "prosecnaOcena": "test",
    },
    {
      "brojIndeksa": "1236/1234567",
      "ime": "Jovana",
      "prezime": "Marković",
      "smer": "SII",
      prosecnaOcena: 10,
    },
    {
      "brojIndeksa": "1237/1234567",
      "ime": "Petar",
      "prezime": "Petrović",
      "smer": "SII",
    },
  ]

  dodajStudenta(indeks: any) {
    if (indeks != undefined) {
      this.studenti[indeks] = { ...this.student };
      this.resetForme();
      return;
    }
    this.studenti.push({ ...this.student });
    return;
  }

  ukloni(indeks: number) {
    this.studenti.splice(indeks, 1);
  }

  izmeni(indeks: number) {
    this.indeks = indeks;
    this.student = { ...this.studenti[indeks] };
  }

  resetForme() {
    this.indeks = undefined;
    this.student = {};
  }
}
