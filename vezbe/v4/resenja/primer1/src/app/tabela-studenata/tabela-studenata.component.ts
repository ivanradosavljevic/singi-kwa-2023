import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tabela-studenata',
  templateUrl: './tabela-studenata.component.html',
  styleUrls: ['./tabela-studenata.component.css']
})
export class TabelaStudenataComponent implements OnInit {

  @Input()
  elementi: any[] = [];

  @Output()
  uklanjanje : EventEmitter<any> = new EventEmitter<any>();

  @Output()
  izmena: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  ukloni(i:any, v:any) {
    this.uklanjanje.emit({index: i, value: {...v}});
  }

  izmeni(i:any, v:any) {
    this.izmena.emit({index: i, value: {...v}});
  }

  sort(v:any) {

  }
}
