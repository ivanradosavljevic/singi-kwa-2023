import { Component } from '@angular/core';
import { StudentiService } from './service/studenti.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'Tabela studenata';

  studentZaIzmenu: any = {};
  parametri : any = {};
  filtriraniStudenti: any[] = [];

  constructor(private studentiServis : StudentiService) {
    this.filtriraniStudenti = studentiServis.pretraziStudente();
  }

  postaviZaIzmenu(red: any) {
    this.studentZaIzmenu = { ...red.value, originalniIndeks: red.value["brojIndeksa"] };
  }

  obradaStudenta(student: any) {
    this.studentiServis.azuriranje(student);
    this.pretraziStudente(this.parametri);
  }

  ukloni(ukloniDogadjaj: any) {
    this.studentiServis.ukloni(ukloniDogadjaj);
    this.pretraziStudente(this.parametri);
  }

  pretraziStudente(parametri : any) {
    if(parametri === undefined) {
      parametri = this.parametri;
    } else {
      this.parametri = parametri;
    }
    this.filtriraniStudenti = this.studentiServis.pretraziStudente(parametri);
  }
}
