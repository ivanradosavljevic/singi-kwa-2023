import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabelaStudenataComponent } from './tabela-studenata/tabela-studenata.component';
import { PretragaStudenataComponent } from './pretraga-studenata/pretraga-studenata.component';
import { StudentFormaComponent } from './student-forma/student-forma.component';

@NgModule({
  declarations: [
    AppComponent,
    TabelaStudenataComponent,
    PretragaStudenataComponent,
    StudentFormaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
