# Vežbe 8
*Cilj vežbi: Rad sa Angular rutiranjem.*

## Zadaci
1. Prethodni primer prepraviti tako da se za svaki entitet obezbedi zasebna stranica za tabelarni prikaz, prikaz pojedinačne instance entiteta, forme za dodavanje i forme za izmenu. Omogućiti dolaženje do svake stranice navigacijom preko linkova.