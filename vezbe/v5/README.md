# Vežbe 5
*Cilj vežbi: Rad sa Angular HTTP Client modulom.*

## Zadaci
1. Instalirati i pokrenuti JSON server.
2. Prepraviti primer sa prethodnih vežbi tako da se sve CRUD operacije realizuju upućivanjem zahveta prethodno podešenom JSON serveru.